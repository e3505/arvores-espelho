#include <malloc.h>
#include <stdio.h>
 

typedef struct arvore {
    char info;
    struct arvore* esq;
    struct arvore* dir;
} Arvore;
 
Arvore* criar_no(int info)
{
    Arvore* novo_no = (Arvore*)malloc(sizeof(Arvore));
    novo_no->info = info;
    novo_no->esq = NULL;
    novo_no->dir = NULL;
    return novo_no;
}
 
void imprime_em_ordem(Arvore* no_raiz)
{
    if (no_raiz == NULL)
        return;
    imprime_em_ordem(no_raiz->esq);
    printf("%d ", no_raiz->info);
    imprime_em_ordem(no_raiz->dir);
}
 
void cria_espelho(Arvore* no_raiz, Arvore** espelho)
{
    if (no_raiz == NULL) {
        espelho = NULL;
        return;
    }
 
    *espelho = criar_no(no_raiz->info);
    cria_espelho(no_raiz->esq, &((*espelho)->dir));
    cria_espelho(no_raiz->dir, &((*espelho)->esq));
}


int eh_espelho(Arvore* a1, Arvore* a2) {

    if(a1->info == a2->info){
        printf("\n1\n");
        return 1;
    } else {
        printf("\n0\n");
        return 0;
    }

    eh_espelho(a1->dir, a2->esq);
    eh_espelho(a2->dir, a1->esq);
}
 
int main()
{
    Arvore* arvore = criar_no(5);
    arvore->esq = criar_no(3);
    arvore->dir = criar_no(6);
    arvore->esq->esq = criar_no(2);
    arvore->esq->dir = criar_no(4);
 
    printf("Ordem da árvore original: ");
    imprime_em_ordem(arvore);
    Arvore* espelho = NULL;
    cria_espelho(arvore, &espelho);
 
    printf("\nOrdem da árvore espelhada: ");
    imprime_em_ordem(espelho);

    Arvore* arvore2 = criar_no(6);
    arvore2->esq = criar_no(7);
    arvore2->dir = criar_no(8);
    arvore2->esq->esq = criar_no(9);
    arvore2->esq->dir = criar_no(1);

    eh_espelho(arvore, arvore2);
 
    return 0;
}